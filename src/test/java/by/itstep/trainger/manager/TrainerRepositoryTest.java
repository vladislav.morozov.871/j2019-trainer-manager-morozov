package by.itstep.trainger.manager;

import by.itstep.trainger.manager.entity.Comment;
import by.itstep.trainger.manager.entity.Recording;
import by.itstep.trainger.manager.entity.Trainer;
import by.itstep.trainger.manager.repository.*;
import by.itstep.trainger.manager.repository.impl.CommentRepositoryImpl;
import by.itstep.trainger.manager.repository.impl.RecordingRepositoryImpl;
import by.itstep.trainger.manager.repository.impl.TrainerRepositoryImpl;
import org.hibernate.Hibernate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TrainerRepositoryTest {

    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();
    private CommentRepository commentRepository = new CommentRepositoryImpl();
    private RecordingRepository recordingRepository = new RecordingRepositoryImpl();

    @BeforeEach
    void setUp() {
        trainerRepository.deleteAll();
    }

    @Test
    void findAll() {
        //GIVEN
        Trainer trainer = Trainer.builder()
                .name("qwe")
                .surname("zxc")
                .build();

        Trainer trainer2 = Trainer.builder()
                .name("zxc")
                .surname("qwe")
                .build();

        trainerRepository.create(trainer);
        trainerRepository.create(trainer2);

        //WHEN
        List<Trainer> list = trainerRepository.findAll();

        //THEN
        Assertions.assertEquals(2, list.size());
    }

    @Test
    void findById() {
        //GIVEN
        Trainer trainer = Trainer.builder()
                .name("qwe")
                .surname("asd")
                .build();

        trainerRepository.create(trainer);

        //WHEN
        Trainer trainer2 = trainerRepository.findById(trainer.getId());

        //THEN
        Assertions.assertEquals(trainer, trainer2);
    }

    @Test
    void create() {
        //GIVEN
        Trainer trainer = Trainer.builder()
                .name("qwe")
                .surname("zxc")
                .build();

        //WHEN
        Trainer saved = trainerRepository.create(trainer);

        //THEN
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void update() {
        //GIVEN
        Trainer trainer1 = Trainer.builder()
                .name("qwe")
                .email("zxc@mail.ru")
                .build();

        Trainer saved = trainerRepository.create(trainer1);

        Trainer trainer2 = Trainer.builder()
                .name("asd")
                .email("qwerty@mail.ru")
                .build();

        //WHEN
        Trainer updateTrainer = trainerRepository.update(trainer2);

        //THEN
        Assertions.assertNotEquals(saved, updateTrainer);
    }

    @Test
    void delete() {
        //GIVEN
        Trainer trainer = Trainer.builder()
                .name("qwerty")
                .surname("zxc")
                .build();
        Trainer saved = trainerRepository.create(trainer);

        //WHEN
        trainerRepository.deleteById(saved.getId());

//        List<Trainer> list = repository.findAll();

        Trainer foundDeleted = trainerRepository.findById(saved.getId());

        //THEN
//        Assertions.assertEquals(list.contains(saved), false);
        Assertions.assertNull(foundDeleted);
    }

    @Test
    void find_ByIdWithCommentsAndRecordings() {
        //GIVEN
        Trainer trainer1 = Trainer.builder()
                .name("qwe")
                .email("asd")
                .build();

        Trainer saved = trainerRepository.create(trainer1);

        Comment comment1 = Comment.builder()
                .name("asd")
                .message("zxc")
                .trainer(saved)
                .build();

        Comment comment2 = Comment.builder()
                .name("qwerty")
                .message("qwe")
                .trainer(saved)
                .build();

        Recording recording1 = Recording.builder()
                .name("qwe")
                .email("zxc")
                .trainer(saved)
                .build();

        Recording recording2 = Recording.builder()
                .name("zxc")
                .email("qwerty")
                .trainer(saved)
                .build();

        commentRepository.create(comment1);
        commentRepository.create(comment2);

        recordingRepository.create(recording1);
        recordingRepository.create(recording2);

        // WHEN
        Trainer found = trainerRepository.findById(saved.getId());

        //THEN
        Assertions.assertNotNull(found);
        Assertions.assertNotNull(found.getId());
        Assertions.assertNotNull(found.getComments());
        Assertions.assertNotNull(found.getRecordings());
        Assertions.assertEquals(2, found.getComments().size());
        Assertions.assertEquals(2, found.getRecordings().size());
    }

    @Test
    void find_AllWithoutCommentsAndRecordings() {
        //GIVEN
        Trainer trainer = Trainer.builder()
                .name("qwe")
                .surname("zxc")
                .build();

        trainerRepository.create(trainer);

        Comment comment1 = Comment.builder()
                .name("qwe")
                .message("zxc")
                .trainer(trainer)
                .build();

        Comment comment2 = Comment.builder()
                .name("asd")
                .message("qwerty")
                .trainer(trainer)
                .build();

        Recording recording1 = Recording.builder()
                .name("asd")
                .trainer(trainer)
                .build();

        Recording recording2 = Recording.builder()
                .name("zxc")
                .trainer(trainer)
                .build();

        commentRepository.create(comment1);
        commentRepository.create(comment2);

        recordingRepository.create(recording1);
        recordingRepository.create(recording2);

        //WHEN
        List<Trainer> list = trainerRepository.findAll();
        for (Trainer trainers : list) {

         //THEN
            Assertions.assertFalse(Hibernate.isInitialized(trainers.getRecordings()));
            Assertions.assertFalse(Hibernate.isInitialized(trainers.getComments()));
        }






    }


}
