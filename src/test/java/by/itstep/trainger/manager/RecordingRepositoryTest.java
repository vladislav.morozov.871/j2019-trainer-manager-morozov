package by.itstep.trainger.manager;

import by.itstep.trainger.manager.entity.Recording;
import by.itstep.trainger.manager.repository.RecordingRepository;
import by.itstep.trainger.manager.repository.impl.RecordingRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class RecordingRepositoryTest {

    private RecordingRepository repository = new RecordingRepositoryImpl();

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void findAll() {
        //GIVEN
        Recording recording = Recording.builder()
                .name("qwe")
                .comment("zxc")
                .build();

        Recording recording2 = Recording.builder()
                .name("zxc")
                .comment("qwe")
                .build();

        repository.create(recording);
        repository.create(recording2);

        //WHEN
        List<Recording> list = repository.findAll();

        //THEN
        Assertions.assertEquals(2, list.size());
    }

    @Test
    void findById() {
        //GIVEN
        Recording recording = Recording.builder()
                .name("asd")
                .comment("qwerty")
                .build();

        repository.create(recording);

        //WHEN
        Recording recording2 = repository.findById(recording.getId());

        //THEN
        Assertions.assertEquals(recording, recording2);
    }

    @Test
    void create() {
        //GIVEN
        Recording recording = Recording.builder()
                .name("qwe")
                .comment("zxc")
                .build();

        //WHEN
        Recording saved = repository.create(recording);

        //THEN
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void update() {
        //GIVEN
        Recording recording1 = Recording.builder()
                .name("qwe")
                .email("asd")
                .build();

        Recording saved = repository.create(recording1);

        Recording recording2 = Recording.builder()
                .name("zxc")
                .email("asd")
                .build();

        //WHEN
        Recording updateRecording = repository.update(recording2);

        //THEN
        Assertions.assertNotEquals(saved, updateRecording);

    }

    @Test
    void delete() {
        //GIVEN
        Recording recording = Recording.builder()
                .name("qwe")
                .comment("asd")
                .build();

        Recording saved = repository.create(recording);

        //WHEN
        repository.deleteById(saved.getId());

        Recording foundDeleted = repository.findById(saved.getId());

        //THEN
        Assertions.assertNull(foundDeleted);

    }

}
