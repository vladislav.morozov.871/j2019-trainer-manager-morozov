package by.itstep.trainger.manager;

import by.itstep.trainger.manager.entity.Comment;
import by.itstep.trainger.manager.repository.CommentRepository;
import by.itstep.trainger.manager.repository.impl.CommentRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CommentRepositoryTest {

    private CommentRepository repository = new CommentRepositoryImpl();

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void findAll() {
        //GIVEN
        Comment comment = Comment.builder()
                .name("qwe")
                .message("asd")
                .build();

        Comment comment2 = Comment.builder()
                .name("zxc")
                .message("asd")
                .build();

        repository.create(comment);
        repository.create(comment2);

        //WHEN
        List<Comment> list = repository.findAll();

        //THEN
        Assertions.assertEquals(2,list.size());
    }

    @Test
    void findById() {
        //GIVEN
        Comment comment = Comment.builder()
                .name("qwe")
                .message("zxc")
                .build();

        repository.create(comment);

        //WHEN
        Comment comment1 = repository.findById(comment.getId());

        //THEN
        Assertions.assertEquals(comment, comment1);
    }

    @Test
    void create() {
        //GIVEN
        Comment comment = Comment.builder()
                .name("qwerty")
                .message("zxc")
                .build();

        //WHEN
        Comment saved = repository.create(comment);

        //THEN
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void update() {
        //GIVEN
        Comment comment1 = Comment.builder()
                .name("qwe")
                .message("asd")
                .build();

        Comment saved = repository.create(comment1);

        Comment comment2 = Comment.builder()
                .name("asd")
                .message("zxc")
                .build();

        //WHEN
        Comment updateComment = repository.update(comment2);

        //THEN
        Assertions.assertNotEquals(saved, updateComment);


    }

    @Test
    void delete() {
        Comment comment = Comment.builder()
                .name("zxc")
                .message("qwerty")
                .build();

        Comment saved = repository.create(comment);

        //WHEN
        repository.deleteById(saved.getId());

        Comment foundDeleted = repository.findById(saved.getId());

        //THEN
        Assertions.assertNull(foundDeleted);

    }


}
