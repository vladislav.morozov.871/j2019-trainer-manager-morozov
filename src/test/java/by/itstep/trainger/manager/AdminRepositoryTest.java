package by.itstep.trainger.manager;

import by.itstep.trainger.manager.entity.Admin;
import by.itstep.trainger.manager.enums.Role;
import by.itstep.trainger.manager.repository.AdminRepository;
import by.itstep.trainger.manager.repository.impl.AdminRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class AdminRepositoryTest {

    private AdminRepository repository = new AdminRepositoryImpl();

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void findAll() {
        //GIVEN
        Admin admin = Admin.builder()
                .name("qwe")
                .surname("asd")
                .role(Role.ADMIN)
                .build();

        Admin admin2 = Admin.builder()
                .name("qwe2")
                .surname("asd2")
                .role(Role.ADMIN)
                .build();

        repository.create(admin);
        repository.create(admin2);

        //WHEN
        List<Admin> list = repository.findAll();

        //THEN
        Assertions.assertEquals(2, list.size());
    }

    @Test
    void findById() {
        //GIVEN
       Admin admin = Admin.builder()
               .name("zxc")
               .surname("qwerty")
               .build();

       repository.create(admin);

        //WHEN
        Admin admin1 = repository.findById(admin.getId());

        //THEN
        Assertions.assertEquals(admin, admin1);

    }

    @Test
    void create() {
        //GIVEN
        Admin admin = Admin.builder()
                .name("qwe")
                .surname("zxc")
                .build();

        //WHEN
        Admin saved = repository.create(admin);

        //THEN
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void update() {
        //GIVEN
        Admin admin1 = Admin.builder()
                .name("asd")
                .email("qwe")
                .build();

        Admin saved = repository.create(admin1);

        Admin admin2 = Admin.builder()
                .name("qwe")
                .email("zxc")
                .build();

        //WHEN
        Admin updateAdmin = repository.update(admin2);

        //THEN
        Assertions.assertNotEquals(saved, updateAdmin);

    }

    @Test
    void delete() {
        //GIVEN
        Admin admin = Admin.builder()
                .name("qwerty")
                .surname("zxczxc")
                .build();
        Admin saved = repository.create(admin);

        //WHEN
        repository.deleteById(saved.getId());

        Admin foundDeleted = repository.findById(saved.getId());

        //THEN
        Assertions.assertNull(foundDeleted);
    }



}
