package by.itstep.trainger.manager.repository.impl;

import by.itstep.trainger.manager.entity.Recording;
import by.itstep.trainger.manager.repository.RecordingRepository;

import javax.persistence.EntityManager;
import java.util.List;

import static by.itstep.trainger.manager.util.EntityManagerUtils.getEntityManager;

public class RecordingRepositoryImpl implements RecordingRepository {

    @Override
    public List<Recording> findAll() {
        EntityManager em = getEntityManager();

        List<Recording> foundList = em.createNativeQuery("SELECT * FROM recording", Recording.class)
                .getResultList();

        em.close();
        return foundList;
    }

    @Override
    public Recording findById(Long id) {
        EntityManager em = getEntityManager();

        Recording foundRecording = em.find(Recording.class, id);

        em.close();
        return foundRecording;
    }

    @Override
    public Recording create(Recording recording) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(recording);

        em.getTransaction().commit();
        em.close();
        return recording;
    }

    @Override
    public Recording update(Recording recording) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.merge(recording);

        em.getTransaction().commit();
        em.close();
        return recording;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        Recording foundRecording = em.find(Recording.class, id);
        em.remove(foundRecording);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM recording").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public List<Recording> findAllByTrainerId(Long id) {
        EntityManager em = getEntityManager();

//        List<Recording> foundList = em.createNativeQuery(
//                String.format("SELECT * FROM recording WHERE trainer_id = ", id), Recording.class)
//                .getResultList();

        List<Recording> foundList = em.createNativeQuery(
                String.format("SELECT * FROM recording WHERE trainer_id = \"%s\"", id), Recording.class)
                .getResultList();

        em.close();
        return foundList;
    }
}
