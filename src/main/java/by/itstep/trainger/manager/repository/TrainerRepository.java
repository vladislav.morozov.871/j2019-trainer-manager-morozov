package by.itstep.trainger.manager.repository;

import by.itstep.trainger.manager.entity.Admin;
import by.itstep.trainger.manager.entity.Recording;
import by.itstep.trainger.manager.entity.Trainer;

import java.util.List;

public interface TrainerRepository {

    Trainer findByEmail(String email);

    List<Trainer> findAll();

    Trainer findById(Long id);

    Trainer create(Trainer trainer);

    Trainer update(Trainer trainer);

    void deleteById(Long id);

    void deleteAll();


}
