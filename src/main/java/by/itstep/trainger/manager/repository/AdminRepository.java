package by.itstep.trainger.manager.repository;

import by.itstep.trainger.manager.entity.Admin;

import java.util.List;

public interface AdminRepository {

    Admin findByEmail(String email);

    List<Admin> findAll();

    Admin findById(Long id);

    Admin create(Admin admin);

    Admin update(Admin admin);

    void deleteById(Long id);

    void deleteAll();

}
