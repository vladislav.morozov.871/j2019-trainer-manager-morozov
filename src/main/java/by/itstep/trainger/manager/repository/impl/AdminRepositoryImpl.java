package by.itstep.trainger.manager.repository.impl;

import by.itstep.trainger.manager.entity.Admin;
import by.itstep.trainger.manager.repository.AdminRepository;

import javax.persistence.EntityManager;
import java.util.List;

import static by.itstep.trainger.manager.util.EntityManagerUtils.getEntityManager;

public class AdminRepositoryImpl implements AdminRepository {
    @Override
    public Admin findByEmail(String email) {
        EntityManager em = getEntityManager();

        Admin foundAdmin = (Admin) em.createNativeQuery(
                String.format("SELECT * FROM admin WHERE email = \"%s\"", email), Admin.class)
                .getSingleResult();

        em.close();

        System.out.println("Found: " + foundAdmin);
        return foundAdmin;
    }

    @Override
    public List<Admin> findAll() {
        EntityManager em = getEntityManager();

        List<Admin> foundList = em.createNativeQuery("SELECT * FROM admin", Admin.class)
                .getResultList();

        em.close();
        return foundList;
    }

    @Override
    public Admin findById(Long id) {
        EntityManager em = getEntityManager();

        Admin foundAdmin = em.find(Admin.class, id);

        em.close();
        return foundAdmin;
    }

    @Override
    public Admin create(Admin admin) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(admin);

        em.getTransaction().commit();
        em.close();
        return admin;
    }

    @Override
    public Admin update(Admin admin) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.merge(admin);

        em.getTransaction().commit();
        em.close();
        return admin;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        Admin foundAdmin = em.find(Admin.class, id);
        em.remove(foundAdmin);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM admin").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
