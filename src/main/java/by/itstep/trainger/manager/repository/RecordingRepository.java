package by.itstep.trainger.manager.repository;

import by.itstep.trainger.manager.entity.Recording;

import java.util.List;

public interface RecordingRepository {

    List<Recording> findAll();

    Recording findById(Long id);

    Recording create(Recording recording);

    Recording update(Recording recording);

    void deleteById(Long id);

    void deleteAll();

    List<Recording> findAllByTrainerId(Long id);
}
