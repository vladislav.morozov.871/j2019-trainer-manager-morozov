package by.itstep.trainger.manager.repository.impl;

import by.itstep.trainger.manager.entity.Trainer;
import by.itstep.trainger.manager.repository.TrainerRepository;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import java.util.List;

import static by.itstep.trainger.manager.util.EntityManagerUtils.getEntityManager;

public class TrainerRepositoryImpl implements TrainerRepository {
    @Override
    public Trainer findByEmail(String email) {
        EntityManager em = getEntityManager();

        Trainer foundTrainer = (Trainer) em.createNativeQuery(
                String.format("SELECT * FROM trainer WHERE email = \"%s\"", email), Trainer.class)
                .getSingleResult();

        em.close();

        System.out.println("Found: " + foundTrainer);
        return foundTrainer;
    }

    @Override
    public List<Trainer> findAll() {
        EntityManager em = getEntityManager();

        List<Trainer> foundList = em.createNativeQuery("SELECT * FROM trainer", Trainer.class)
                .getResultList();

        em.close();
        return foundList;
    }

    @Override
    public Trainer findById(Long id) {
        EntityManager em = getEntityManager();

        Trainer foundTrainer = em.find(Trainer.class, id);

        if (foundTrainer != null) {
            Hibernate.initialize(foundTrainer.getComments());
            Hibernate.initialize(foundTrainer.getRecordings());
        }
        em.close();
        return foundTrainer;
    }

    @Override
    public Trainer create(Trainer trainer) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(trainer);

        em.getTransaction().commit();
        em.close();
        return trainer;
    }

    @Override
    public Trainer update(Trainer trainer) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.merge(trainer);

        em.getTransaction().commit();
        em.close();
        return trainer;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        Trainer foundTrainer = em.find(Trainer.class, id);
        em.remove(foundTrainer);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM trainer").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
