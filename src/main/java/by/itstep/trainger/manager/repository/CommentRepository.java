package by.itstep.trainger.manager.repository;

import by.itstep.trainger.manager.entity.Admin;
import by.itstep.trainger.manager.entity.Comment;
import by.itstep.trainger.manager.entity.Recording;

import java.util.List;

public interface CommentRepository {

    List<Comment> findAll();

    Comment findById(Long id);

    Comment create(Comment comment);

    Comment update(Comment comment);

    void deleteById(Long id);

    void deleteAll();

    List<Comment> findAllByTrainerId(Long id);

}
