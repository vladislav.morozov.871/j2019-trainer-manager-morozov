package by.itstep.trainger.manager.repository.impl;

import by.itstep.trainger.manager.entity.Admin;
import by.itstep.trainger.manager.entity.Comment;
import by.itstep.trainger.manager.entity.Recording;
import by.itstep.trainger.manager.repository.CommentRepository;

import javax.persistence.EntityManager;
import java.util.List;

import static by.itstep.trainger.manager.util.EntityManagerUtils.getEntityManager;

public class CommentRepositoryImpl implements CommentRepository {

    @Override
    public List<Comment> findAll() {
        EntityManager em = getEntityManager();

        List<Comment> foundList = em.createNativeQuery("SELECT * FROM comment", Comment.class)
                .getResultList();

        em.close();
        return foundList;
    }

    @Override
    public Comment findById(Long id) {
        EntityManager em = getEntityManager();

        Comment foundComment = em.find(Comment.class, id);

        em.close();
        return foundComment;
    }

    @Override
    public Comment create(Comment comment) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(comment);

        em.getTransaction().commit();
        em.close();
        return comment;
    }

    @Override
    public Comment update(Comment comment) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.merge(comment);

        em.getTransaction().commit();
        em.close();
        return comment;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        Comment foundComment = em.find(Comment.class, id);
        em.remove(foundComment);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM comment").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public List<Comment> findAllByTrainerId(Long id) {
        EntityManager em = getEntityManager();

        List<Comment> foundList = em.createNativeQuery(
                String.format("SELECT * FROM comment WHERE trainer_id = \"%s\"", id), Comment.class)
                .getResultList();

        em.close();
        return foundList;
    }
}
