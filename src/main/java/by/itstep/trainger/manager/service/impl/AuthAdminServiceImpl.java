package by.itstep.trainger.manager.service.impl;

import by.itstep.trainger.manager.dto.adminDto.AdminFullDto;
import by.itstep.trainger.manager.dto.adminDto.AdminLoginDto;
import by.itstep.trainger.manager.entity.Admin;
import by.itstep.trainger.manager.enums.Role;
import by.itstep.trainger.manager.repository.AdminRepository;
import by.itstep.trainger.manager.repository.impl.AdminRepositoryImpl;
import by.itstep.trainger.manager.service.AdminService;
import by.itstep.trainger.manager.service.AuthAdminService;

public class AuthAdminServiceImpl implements AuthAdminService {

    private Admin loginedAdmin;
    private AdminRepository adminRepository = new AdminRepositoryImpl();


    @Override
    public void login(AdminLoginDto loginDto) {
        Admin foundAdmin = adminRepository.findByEmail(loginDto.getEmail());
        if (foundAdmin == null) {
            throw new RuntimeException("Admin not found by email: " + loginDto.getEmail());
        }

        if (!foundAdmin.getPassword().equals(loginDto.getPassword())) {
            throw new RuntimeException("User password is incorrect");
        }

        loginedAdmin = foundAdmin;
    }

    @Override
    public void logOut() {
        loginedAdmin = null;
    }

    @Override
    public boolean isAuthenticated() {
        return loginedAdmin != null;
    }

    @Override
    public Role getRole() {
        return loginedAdmin.getRole();
    }

    @Override
    public Admin getLoginedAdmin() {
        return loginedAdmin;
    }
}
