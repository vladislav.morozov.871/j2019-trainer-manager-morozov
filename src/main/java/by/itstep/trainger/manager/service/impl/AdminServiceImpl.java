package by.itstep.trainger.manager.service.impl;

import by.itstep.trainger.manager.dto.adminDto.AdminCreateDto;
import by.itstep.trainger.manager.dto.adminDto.AdminFullDto;
import by.itstep.trainger.manager.dto.adminDto.AdminPreviewDto;
import by.itstep.trainger.manager.dto.adminDto.AdminUpdateDto;
import by.itstep.trainger.manager.entity.Admin;
import by.itstep.trainger.manager.mapper.AdminMapper;
import by.itstep.trainger.manager.repository.AdminRepository;
import by.itstep.trainger.manager.repository.impl.AdminRepositoryImpl;
import by.itstep.trainger.manager.service.AdminService;

import java.util.List;

public class AdminServiceImpl implements AdminService {

    AdminRepository adminRepository = new AdminRepositoryImpl();

    AdminMapper mapper = new AdminMapper();

    @Override
    public AdminFullDto findByEmail(String email) {
        Admin found = adminRepository.findByEmail(email);
        return mapper.mapToDto(found);
    }

    @Override
    public List<AdminPreviewDto> findAll() {
        List<Admin> found = adminRepository.findAll();
        return mapper.mapToDtoList(found);
    }

    @Override
    public AdminFullDto findById(Long id) {
        Admin found = adminRepository.findById(id);
        return mapper.mapToDto(found);
    }

    @Override
    public AdminFullDto create(AdminCreateDto createDto) {
        Admin toSave = mapper.mapToEntity(createDto);

        Admin created = adminRepository.create(toSave);

        return mapper.mapToDto(created) ;
    }

    @Override
    public AdminFullDto update(AdminUpdateDto updateDto) {
        Admin toUpdate = mapper.mapToEntity(updateDto);

        Admin existingEntity = adminRepository.findById(updateDto.getId());

        toUpdate.setRole(existingEntity.getRole());

        Admin updated = adminRepository.update(toUpdate);

        return mapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        adminRepository.deleteById(id);
    }
}
