package by.itstep.trainger.manager.service.impl;

import by.itstep.trainger.manager.dto.trainerDto.TrainerCreateDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerFullDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerPreviewDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerUpdateDto;
import by.itstep.trainger.manager.entity.Trainer;
import by.itstep.trainger.manager.mapper.TrainerMapper;
import by.itstep.trainger.manager.repository.TrainerRepository;
import by.itstep.trainger.manager.repository.impl.TrainerRepositoryImpl;
import by.itstep.trainger.manager.service.TrainerService;

import java.util.List;

public class TrainerServiceImpl implements TrainerService {

    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    private TrainerMapper mapper = new TrainerMapper();

    @Override
    public List<TrainerPreviewDto> findAll() {
        List<Trainer> found = trainerRepository.findAll();
        return mapper.mapToDtoList(found);
    }

    @Override
    public TrainerFullDto findById(Long id) {
        Trainer found = trainerRepository.findById(id);

        return mapper.mapToDto(found);
    }

    @Override
    public TrainerFullDto create(TrainerCreateDto createDto) {
        Trainer toSave = mapper.mapToEntity(createDto);

        Trainer created = trainerRepository.create(toSave);

        return mapper.mapToDto(created);
    }

    @Override
    public TrainerFullDto update(TrainerUpdateDto updateDto) {
        Trainer toUpdate = mapper.mapToEntity(updateDto);

        Trainer existingEntity = trainerRepository.findById(updateDto.getId());

        toUpdate.setEmail(existingEntity.getEmail());

        Trainer updated = trainerRepository.update(toUpdate);

        return mapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        trainerRepository.deleteById(id);
    }
}
