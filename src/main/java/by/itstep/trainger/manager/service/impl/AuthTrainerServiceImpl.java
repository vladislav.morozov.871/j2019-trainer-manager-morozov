package by.itstep.trainger.manager.service.impl;

import by.itstep.trainger.manager.dto.trainerDto.TrainerLoginDto;
import by.itstep.trainger.manager.entity.Trainer;
import by.itstep.trainger.manager.repository.TrainerRepository;
import by.itstep.trainger.manager.repository.impl.TrainerRepositoryImpl;
import by.itstep.trainger.manager.service.AuthTrainerService;

public class AuthTrainerServiceImpl implements AuthTrainerService {

    private Trainer loginedTrainer;
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    @Override
    public void login(TrainerLoginDto loginDto) {
        Trainer foundTrainer = trainerRepository.findByEmail(loginDto.getEmail());
        if(foundTrainer == null) {
            throw new RuntimeException("Trainer not found by email: " + loginDto.getEmail());
        }

        if(!foundTrainer.getPassword().equals(loginDto.getPassword())) {
            throw new RuntimeException("Trainer password is incorrect");
        }

        loginedTrainer = foundTrainer;
    }

    @Override
    public void logOut() {
        loginedTrainer = null;
    }

    @Override
    public boolean isAuthenticated() {
        return loginedTrainer != null;
    }

    @Override
    public Trainer getLoginedTrainer() {
        return loginedTrainer;
    }
}
