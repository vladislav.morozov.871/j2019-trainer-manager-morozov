package by.itstep.trainger.manager.service;

import by.itstep.trainger.manager.dto.commentDto.CommentCreateDto;
import by.itstep.trainger.manager.dto.commentDto.CommentFullDto;
import by.itstep.trainger.manager.dto.commentDto.CommentPreviewDto;
import by.itstep.trainger.manager.dto.commentDto.CommentUpdateDto;
import by.itstep.trainger.manager.entity.Comment;

import java.util.List;

public interface CommentService {

    List<CommentPreviewDto> findAll();

    CommentFullDto findById(Long id);

    CommentFullDto create(CommentCreateDto createDto);

    CommentFullDto update(CommentUpdateDto updateDto);

    void deleteById(Long id);

    List<CommentPreviewDto> findAllByTrainerId(Long id);

}
