package by.itstep.trainger.manager.service.impl;

import by.itstep.trainger.manager.dto.recordingDto.RecordingCreateDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingFullDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingPreviewDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingUpdateDto;
import by.itstep.trainger.manager.entity.Recording;
import by.itstep.trainger.manager.entity.Trainer;
import by.itstep.trainger.manager.mapper.RecordingMapper;
import by.itstep.trainger.manager.repository.RecordingRepository;
import by.itstep.trainger.manager.repository.impl.RecordingRepositoryImpl;
import by.itstep.trainger.manager.repository.TrainerRepository;
import by.itstep.trainger.manager.repository.impl.TrainerRepositoryImpl;
import by.itstep.trainger.manager.service.RecordingService;

import java.util.List;

public class RecordingServiceImpl implements RecordingService {

    private RecordingRepository recordingRepository = new RecordingRepositoryImpl();

    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    private RecordingMapper mapper = new RecordingMapper();

    @Override
    public List<RecordingPreviewDto> findAll() {
        List<Recording> found = recordingRepository.findAll();

        return mapper.mapToDtoList(found);
    }

    @Override
    public RecordingFullDto findById(Long id) {
        Recording found = recordingRepository.findById(id);

        return mapper.mapToDto(found);
    }

    @Override
    public RecordingFullDto create(RecordingCreateDto createDto) {
        Trainer trainer = trainerRepository.findById(createDto.getTrainerId());
        Recording toSave = mapper.mapToEntity(createDto, trainer);

        Recording created = recordingRepository.create(toSave);

        return mapper.mapToDto(created);
    }

    @Override
    public RecordingFullDto update(RecordingUpdateDto updateDto) {
        Recording toUpdate = mapper.mapToEntity(updateDto);
        Recording existingEntity = recordingRepository.findById(updateDto.getId());

        toUpdate.setRecordingTime(existingEntity.getRecordingTime());
        toUpdate.setTrainer(existingEntity.getTrainer());

        Recording updated = recordingRepository.update(toUpdate);

        return mapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        recordingRepository.deleteById(id);
    }

    @Override
    public List<RecordingPreviewDto> findAllByTrainerId(Long id) {
        List<Recording> recordings = recordingRepository.findAllByTrainerId(id);

        return mapper.mapToDtoList(recordings);

    }

}
