package by.itstep.trainger.manager.service;

import by.itstep.trainger.manager.dto.adminDto.AdminCreateDto;
import by.itstep.trainger.manager.dto.adminDto.AdminFullDto;
import by.itstep.trainger.manager.dto.adminDto.AdminPreviewDto;
import by.itstep.trainger.manager.dto.adminDto.AdminUpdateDto;
import by.itstep.trainger.manager.entity.Admin;

import java.util.List;

public interface AdminService {

    AdminFullDto findByEmail(String email);

    List<AdminPreviewDto> findAll();

    AdminFullDto findById(Long id);

    AdminFullDto create(AdminCreateDto createDto);

    AdminFullDto update(AdminUpdateDto updateDto);

    void deleteById(Long id);
}
