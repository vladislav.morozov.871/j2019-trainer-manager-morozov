package by.itstep.trainger.manager.service;

import by.itstep.trainger.manager.dto.adminDto.AdminLoginDto;
import by.itstep.trainger.manager.entity.Admin;
import by.itstep.trainger.manager.enums.Role;

public interface AuthAdminService {

    void login(AdminLoginDto loginDto);

    void logOut();

    boolean isAuthenticated();

    Role getRole();

    Admin getLoginedAdmin();

}
