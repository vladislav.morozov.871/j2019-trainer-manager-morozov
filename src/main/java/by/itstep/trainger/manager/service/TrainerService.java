package by.itstep.trainger.manager.service;

import by.itstep.trainger.manager.dto.trainerDto.TrainerCreateDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerFullDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerPreviewDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerUpdateDto;

import java.util.List;

public interface TrainerService {

    List<TrainerPreviewDto> findAll();

    TrainerFullDto findById(Long id);

    TrainerFullDto create(TrainerCreateDto createDto);

    TrainerFullDto update(TrainerUpdateDto updateDto);

    void deleteById(Long id);
}
