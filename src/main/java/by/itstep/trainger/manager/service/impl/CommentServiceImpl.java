package by.itstep.trainger.manager.service.impl;

import by.itstep.trainger.manager.dto.commentDto.CommentCreateDto;
import by.itstep.trainger.manager.dto.commentDto.CommentFullDto;
import by.itstep.trainger.manager.dto.commentDto.CommentPreviewDto;
import by.itstep.trainger.manager.dto.commentDto.CommentUpdateDto;
import by.itstep.trainger.manager.entity.Comment;
import by.itstep.trainger.manager.entity.Trainer;
import by.itstep.trainger.manager.mapper.CommentMapper;
import by.itstep.trainger.manager.repository.CommentRepository;
import by.itstep.trainger.manager.repository.impl.CommentRepositoryImpl;
import by.itstep.trainger.manager.repository.TrainerRepository;
import by.itstep.trainger.manager.repository.impl.TrainerRepositoryImpl;
import by.itstep.trainger.manager.service.CommentService;

import java.util.List;

public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository = new CommentRepositoryImpl();

    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    private CommentMapper mapper = new CommentMapper();

    @Override
    public List<CommentPreviewDto> findAll() {
        List<Comment> found = commentRepository.findAll();

        return mapper.mapToDtoList(found);
    }

    @Override
    public CommentFullDto findById(Long id) {
        Comment found = commentRepository.findById(id);

        return mapper.mapToDto(found);
    }

    @Override
    public CommentFullDto create(CommentCreateDto createDto) {
        Trainer trainer = trainerRepository.findById(createDto.getTrainerId());
        Comment toSave = mapper.mapToEntity(createDto, trainer);

        Comment created = commentRepository.create(toSave);

        return mapper.mapToDto(created);
    }

    @Override
    public CommentFullDto update(CommentUpdateDto updateDto) {
        Comment toUpdate = mapper.mapToEntity(updateDto);
        Comment existingEntity = commentRepository.findById(updateDto.getId());

        toUpdate.setName(existingEntity.getName());
        toUpdate.setSurname(existingEntity.getSurname());
        toUpdate.setEmail(existingEntity.getEmail());
        toUpdate.setTrainer(existingEntity.getTrainer());

        Comment updated = commentRepository.update(toUpdate);

        return mapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        commentRepository.deleteById(id);
    }

    @Override
    public List<CommentPreviewDto> findAllByTrainerId(Long id) {
        List<Comment> comments = commentRepository.findAllByTrainerId(id);

        return mapper.mapToDtoList(comments);
    }
}
