package by.itstep.trainger.manager.service;

import by.itstep.trainger.manager.dto.recordingDto.RecordingCreateDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingFullDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingPreviewDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingUpdateDto;

import java.util.List;

public interface RecordingService {

    List<RecordingPreviewDto> findAll();

    RecordingFullDto findById(Long id);

    RecordingFullDto create(RecordingCreateDto createDto);

    RecordingFullDto update(RecordingUpdateDto updateDto);

    void deleteById(Long id);

    List<RecordingPreviewDto> findAllByTrainerId(Long id);

}
