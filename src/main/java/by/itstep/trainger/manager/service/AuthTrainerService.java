package by.itstep.trainger.manager.service;

import by.itstep.trainger.manager.dto.adminDto.AdminLoginDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerLoginDto;
import by.itstep.trainger.manager.entity.Admin;
import by.itstep.trainger.manager.entity.Trainer;
import by.itstep.trainger.manager.enums.Role;

public interface AuthTrainerService {

    void login(TrainerLoginDto loginDto);

    void logOut();

    boolean isAuthenticated();

    Trainer getLoginedTrainer();

}
