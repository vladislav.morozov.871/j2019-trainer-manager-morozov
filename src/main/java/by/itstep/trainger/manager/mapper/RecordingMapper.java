package by.itstep.trainger.manager.mapper;

import by.itstep.trainger.manager.dto.recordingDto.RecordingCreateDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingFullDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingPreviewDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingUpdateDto;
import by.itstep.trainger.manager.entity.Recording;
import by.itstep.trainger.manager.entity.Trainer;

import java.util.ArrayList;
import java.util.List;

public class RecordingMapper {

    public List<RecordingPreviewDto> mapToDtoList(List<Recording> entities) {
        List<RecordingPreviewDto> dtos = new ArrayList<>();

        for (Recording entity : entities) {
            RecordingPreviewDto dto = new RecordingPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setSurname(entity.getSurname());
            dto.setRecordingTime(entity.getRecordingTime());
            dto.setTrainer(entity.getTrainer().getName() + " " + entity.getTrainer().getSurname());

            dtos.add(dto);
        }
        return dtos;
    }

    public Recording mapToEntity(RecordingCreateDto createDto, Trainer trainer) {
        Recording recording = new Recording();
        recording.setRecordingTime(createDto.getRecordingTime());
        recording.setComment(createDto.getComment());
        recording.setName(createDto.getName());
        recording.setSurname(createDto.getSurname());
        recording.setEmail(createDto.getEmail());
        recording.setPhone(createDto.getPhone());
        recording.setTrainer(trainer);

        return recording;
    }

    public Recording mapToEntity(RecordingUpdateDto updateDto) {
        Recording recording = new Recording();
        recording.setId(updateDto.getId());
        recording.setComment(updateDto.getComment());
        recording.setName(updateDto.getName());
        recording.setSurname(updateDto.getSurname());
        recording.setEmail(updateDto.getEmail());
        recording.setPhone(updateDto.getPhone());

        return recording;
    }

    public RecordingFullDto mapToDto(Recording entity) {
        RecordingFullDto fullDto = new RecordingFullDto();
        fullDto.setId(entity.getId());
        fullDto.setTrainer(entity.getTrainer());
        fullDto.setRecordingTime(entity.getRecordingTime());
        fullDto.setComment(entity.getComment());
        fullDto.setName(entity.getName());
        fullDto.setSurname(entity.getSurname());
        fullDto.setEmail(entity.getEmail());
        fullDto.setPhone(entity.getPhone());

        return fullDto;
    }

}
