package by.itstep.trainger.manager.mapper;

import by.itstep.trainger.manager.dto.adminDto.AdminCreateDto;
import by.itstep.trainger.manager.dto.adminDto.AdminFullDto;
import by.itstep.trainger.manager.dto.adminDto.AdminPreviewDto;
import by.itstep.trainger.manager.dto.adminDto.AdminUpdateDto;
import by.itstep.trainger.manager.entity.Admin;

import java.util.ArrayList;
import java.util.List;

public class AdminMapper {

    public List<AdminPreviewDto> mapToDtoList(List<Admin> entities) {
        List<AdminPreviewDto> dtos = new ArrayList<>();

        for (Admin entity : entities) {
            AdminPreviewDto dto = new AdminPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setSurname(entity.getSurname());
            dto.setRole(entity.getRole());

            dtos.add(dto);
        }
        return dtos;
    }

    public Admin mapToEntity(AdminCreateDto createDto) {
        Admin admin = new Admin();
        admin.setName(createDto.getName());
        admin.setSurname(createDto.getSurname());
        admin.setEmail(createDto.getEmail());
        admin.setPassword(createDto.getPassword());

        return admin;
    }

    public Admin mapToEntity(AdminUpdateDto updateDto) {
        Admin admin = new Admin();
        admin.setId(updateDto.getId());
        admin.setName(updateDto.getName());
        admin.setSurname(updateDto.getSurname());
        admin.setEmail(updateDto.getEmail());
        admin.setPassword(updateDto.getPassword());

        return admin;
    }

    public AdminFullDto mapToDto(Admin entity) {
        AdminFullDto fullDto = new AdminFullDto();
        fullDto.setId(entity.getId());
        fullDto.setName(entity.getName());
        fullDto.setSurname(entity.getSurname());
        fullDto.setEmail(entity.getEmail());
        fullDto.setRole(entity.getRole());

        return fullDto;
    }

}
