package by.itstep.trainger.manager.mapper;

import by.itstep.trainger.manager.dto.commentDto.CommentCreateDto;
import by.itstep.trainger.manager.dto.commentDto.CommentFullDto;
import by.itstep.trainger.manager.dto.commentDto.CommentPreviewDto;
import by.itstep.trainger.manager.dto.commentDto.CommentUpdateDto;
import by.itstep.trainger.manager.entity.Comment;
import by.itstep.trainger.manager.entity.Trainer;

import java.util.ArrayList;
import java.util.List;

public class CommentMapper {

    public List<CommentPreviewDto> mapToDtoList(List<Comment> entities) {
        List<CommentPreviewDto> dtos = new ArrayList<>();

        for (Comment entity : entities) {
            CommentPreviewDto dto = new CommentPreviewDto();

            dto.setId(entity.getId());
            dto.setTrainer(entity.getTrainer());
            dto.setMessage(entity.getMessage());
            dto.setName(entity.getName());
            dto.setSurname(entity.getSurname());
            dto.setEmail(entity.getEmail());
            dto.setMark(entity.getMark());
            dto.setPublished(entity.isPublished());

            dtos.add(dto);
        }
        return dtos;
    }

    public Comment mapToEntity(CommentCreateDto createDto, Trainer trainer) {
        Comment comment = new Comment();
        comment.setMessage(createDto.getMessage());
        comment.setName(createDto.getName());
        comment.setSurname(createDto.getSurname());
        comment.setEmail(createDto.getEmail());
        comment.setMark(createDto.getMark());
        comment.setTrainer(trainer);

        return comment;
    }

    public Comment mapToEntity(CommentUpdateDto updateDto) {
        Comment comment = new Comment();
        comment.setId(updateDto.getId());
        comment.setMessage(updateDto.getMessage());
        comment.setMark(updateDto.getMark());

        return comment;
    }

    public CommentFullDto mapToDto(Comment entity) {
        CommentFullDto fullDto = new CommentFullDto();
        fullDto.setId(entity.getId());
        fullDto.setTrainer(entity.getTrainer());
        fullDto.setMessage(entity.getMessage());
        fullDto.setName(entity.getName());
        fullDto.setSurname(entity.getSurname());
        fullDto.setEmail(entity.getEmail());
        fullDto.setMark(entity.getMark());
        fullDto.setPublished(entity.isPublished());

        return fullDto;
    }


}
