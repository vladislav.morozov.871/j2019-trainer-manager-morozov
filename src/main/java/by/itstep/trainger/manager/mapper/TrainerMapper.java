package by.itstep.trainger.manager.mapper;

import by.itstep.trainger.manager.dto.trainerDto.TrainerCreateDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerFullDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerPreviewDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerUpdateDto;
import by.itstep.trainger.manager.entity.Trainer;

import java.util.ArrayList;
import java.util.List;

public class TrainerMapper {

    public List<TrainerPreviewDto> mapToDtoList(List<Trainer> entities) {
        List<TrainerPreviewDto> dtos = new ArrayList<>();

        for (Trainer entity : entities) {
            TrainerPreviewDto dto = new TrainerPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setSurname(entity.getSurname());
            dto.setEmail(entity.getEmail());
            dto.setExperience(entity.getExperience());

            dtos.add(dto);
        }
        return dtos;
    }

    public Trainer mapToEntity(TrainerCreateDto createDto) {
        Trainer trainer = new Trainer();
        trainer.setName(createDto.getName());
        trainer.setSurname(createDto.getSurname());
        trainer.setExperience(createDto.getExperience());
        trainer.setAchievements(createDto.getAchievements());
        trainer.setExperience(createDto.getExperience());
        trainer.setEmail(createDto.getEmail());
        trainer.setPassword(createDto.getPassword());

        return trainer;
    }

    public Trainer mapToEntity(TrainerUpdateDto updateDto) {
        Trainer trainer = new Trainer();
        trainer.setId(updateDto.getId());
        trainer.setName(updateDto.getName());
        trainer.setSurname(updateDto.getSurname());
        trainer.setAchievements(updateDto.getAchievements());
        trainer.setExperience(updateDto.getExperience());
        trainer.setPassword(updateDto.getPassword());

        return trainer;
    }

    public TrainerFullDto mapToDto(Trainer entity) {
        TrainerFullDto fullDto = new TrainerFullDto();
        fullDto.setId(entity.getId());
        fullDto.setName(entity.getName());
        fullDto.setSurname(entity.getSurname());
        fullDto.setExperience(entity.getExperience());
        fullDto.setAchievements(entity.getAchievements());
        fullDto.setComments(entity.getComments());
        fullDto.setEmail(entity.getEmail());
        fullDto.setRecordings(entity.getRecordings());

        return fullDto;
    }

}
