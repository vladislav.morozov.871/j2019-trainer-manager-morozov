package by.itstep.trainger.manager.controller;

import by.itstep.trainger.manager.dto.commentDto.CommentCreateDto;
import by.itstep.trainger.manager.dto.commentDto.CommentFullDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerFullDto;
import by.itstep.trainger.manager.service.CommentService;
import by.itstep.trainger.manager.service.impl.CommentServiceImpl;
import by.itstep.trainger.manager.service.TrainerService;
import by.itstep.trainger.manager.service.impl.TrainerServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommentController {

    private CommentService commentService = new CommentServiceImpl();

    private TrainerService trainerService = new TrainerServiceImpl();

    @RequestMapping(method = RequestMethod.GET, value = "trainer/comment/create/{id}")
    public String openCreationForm(@PathVariable Long id, Model model) {
        TrainerFullDto foundTrainer = trainerService.findById(id);
        model.addAttribute("trainer", foundTrainer);
        model.addAttribute("createDto", new CommentCreateDto());
        return "create-comment";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/comments/create")
    public String saveRecording(CommentCreateDto createDto) {
        commentService.create(createDto);
        return "redirect:/index";
    }



}
