package by.itstep.trainger.manager.controller;

import by.itstep.trainger.manager.dto.recordingDto.RecordingCreateDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingFullDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingPreviewDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerFullDto;
import by.itstep.trainger.manager.service.RecordingService;
import by.itstep.trainger.manager.service.impl.RecordingServiceImpl;
import by.itstep.trainger.manager.service.TrainerService;
import by.itstep.trainger.manager.service.impl.TrainerServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class RecordingController {

    private RecordingService recordingService = new RecordingServiceImpl();

    private TrainerService trainerService = new TrainerServiceImpl();



    @RequestMapping(method = RequestMethod.GET, value = "/recordings")
    public String openRecordings(Model model) {
        List<RecordingPreviewDto> found = recordingService.findAll();

        model.addAttribute("all_recordings", found);
        return "recordings";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/recordings/{id}")
    public String openRecording(@PathVariable Long id, Model model) {
        RecordingFullDto foundRecording = recordingService.findById(id);
        model.addAttribute("recording", foundRecording);

        return "recording";
    }

    @RequestMapping(method = RequestMethod.GET, value = "trainer/recording/create/{id}")
    public String openRecordingForm(@PathVariable Long id, Model model) {
        TrainerFullDto foundTrainer = trainerService.findById(id);
        model.addAttribute("trainer", foundTrainer);
        model.addAttribute("createDto", new RecordingCreateDto());
        return "create-recording";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/recordings/create")
    public String saveRecording(RecordingCreateDto createDto) {
        recordingService.create(createDto);
        return "redirect:/index";
    }



}
