package by.itstep.trainger.manager.controller;

import by.itstep.trainger.manager.dto.adminDto.AdminLoginDto;
import by.itstep.trainger.manager.dto.commentDto.CommentPreviewDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingPreviewDto;
import by.itstep.trainger.manager.dto.trainerDto.*;
import by.itstep.trainger.manager.service.*;
import by.itstep.trainger.manager.service.impl.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class TrainerController {

    private TrainerService trainerService = new TrainerServiceImpl();

    private RecordingService recordingService = new RecordingServiceImpl();

    private CommentService commentService = new CommentServiceImpl();

    private AuthTrainerService authTrainerService = new AuthTrainerServiceImpl();

    private AuthAdminService authAdminService = new AuthAdminServiceImpl();





    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String openMainPage(Model model) {
        List<TrainerPreviewDto> found = trainerService.findAll();

        model.addAttribute("all_trainers", found);
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainer/{id}")
    public String openProfile(@PathVariable Long id, Model model) {
            TrainerFullDto foundTrainer = trainerService.findById(id);
            List<RecordingPreviewDto> recordings = recordingService.findAllByTrainerId(id);
            List<CommentPreviewDto> comments = commentService.findAllByTrainerId(id);
            model.addAttribute("trainer", foundTrainer);
            model.addAttribute("recordings", recordings);
            model.addAttribute("comments", comments);
            return "trainer";
    }

//    @RequestMapping(method = RequestMethod.GET, value = "/registration")
//    public String openCreateTrainer(Model model) {
//        model.addAttribute("createDto", new TrainerCreateDto());
//        return "create-trainer";
//    }
    @RequestMapping(method = RequestMethod.GET, value = "/registration")
    public String openCreateTrainer(Model model) {
        model.addAttribute("createDto", new TrainerCreateDto());
        return "create-trainer";
    }


    @RequestMapping(method = RequestMethod.POST, value = "/trainer/create")
    public String saveTrainer(TrainerCreateDto createDto) {
        TrainerFullDto created = trainerService.create(createDto);
        return "redirect:/trainer/" + created.getId();
    }


    @RequestMapping(method = RequestMethod.GET, value = "/trainer/sign-in")
    public String loginTrainer(Model model) {
        model.addAttribute("loginDto", new AdminLoginDto());
        return "trainer-sign-in";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/trainer/login")
    public String loginTrainer(TrainerLoginDto loginDto) {
        authTrainerService.login(loginDto);

        return "redirect:/index-for-trainer";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/index-for-trainer")
    public String openMainPageByLoginedTrainer(Model model) {
        if (!authTrainerService.isAuthenticated()) {
            return "redirect:/trainer-invalid-login";
        }

        List<TrainerPreviewDto> found = trainerService.findAll();

        model.addAttribute("all_trainers", found);
        model.addAttribute("logined_trainer", authTrainerService.getLoginedTrainer());
        return "index-for-trainer";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainer-invalid-login")
    public String showTrainerLoginException() {
        return "exception";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainer/logout")
    public String logOut() {
        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/trainer/logout")
    public String logout() {
        authTrainerService.logOut();
        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainer/delete/{id}")
    public String deleteTrainer(@PathVariable Long id) {
        trainerService.deleteById(id);
        return "redirect:/index-for-admin";
    }

















}
