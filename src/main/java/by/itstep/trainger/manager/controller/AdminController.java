package by.itstep.trainger.manager.controller;

import by.itstep.trainger.manager.dto.adminDto.AdminCreateDto;
import by.itstep.trainger.manager.dto.adminDto.AdminFullDto;
import by.itstep.trainger.manager.dto.adminDto.AdminLoginDto;
import by.itstep.trainger.manager.dto.trainerDto.TrainerPreviewDto;
import by.itstep.trainger.manager.service.AdminService;
import by.itstep.trainger.manager.service.AuthAdminService;
import by.itstep.trainger.manager.service.TrainerService;
import by.itstep.trainger.manager.service.impl.AdminServiceImpl;
import by.itstep.trainger.manager.service.impl.AuthAdminServiceImpl;
import by.itstep.trainger.manager.service.impl.TrainerServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class AdminController {

    private AdminService adminService = new AdminServiceImpl();

    private AuthAdminService authAdminService = new AuthAdminServiceImpl();

    private TrainerService trainerService = new TrainerServiceImpl();


    @RequestMapping(method = RequestMethod.GET, value = "/admin/registration")
    public String openRegistrationPage(Model model) {
        model.addAttribute("createDto", new AdminCreateDto());
        return "admin-registration";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/create")
    public String saveAdmin(AdminCreateDto createDto) {
        adminService.create(createDto);
        return "redirect:/index-for-admin";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/admin/sign-in")
    public String loginAdmin(Model model) {
        model.addAttribute("loginDto", new AdminLoginDto());
        return "admin-sign-in";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/login")
    public String login(AdminLoginDto loginDto) {
        authAdminService.login(loginDto);

        return "redirect:/index-for-admin";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/index-for-admin")
    public String openMainPage(Model model) {
        if (!authAdminService.isAuthenticated()) {
            return "redirect:/oops";
        }

        List<TrainerPreviewDto> found = trainerService.findAll();

        model.addAttribute("all_trainers", found);
        model.addAttribute("logined_admin", authAdminService.getLoginedAdmin());
        return "index-for-admin";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/oops")
    public String showErrorPage() {
        return "exception";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/admin/logout")
    public String logOut() {
        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/logout")
    public String logout() {
        authAdminService.logOut();
        return "redirect:/index";
    }
}
