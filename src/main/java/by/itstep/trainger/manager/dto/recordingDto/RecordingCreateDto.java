package by.itstep.trainger.manager.dto.recordingDto;

import by.itstep.trainger.manager.entity.Trainer;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;


import javax.persistence.Temporal;
import java.util.Date;

@Data
public class RecordingCreateDto {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date recordingTime;

    private String comment;

    private String name;

    private String surname;

    private String email;

    private int phone;

    private Long trainerId;

}
