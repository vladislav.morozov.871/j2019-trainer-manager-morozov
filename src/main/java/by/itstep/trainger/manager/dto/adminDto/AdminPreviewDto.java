package by.itstep.trainger.manager.dto.adminDto;

import by.itstep.trainger.manager.enums.Role;
import lombok.Data;

@Data
public class AdminPreviewDto {

    private Long id;

    private String name;

    private String surname;

    private Role role;

}
