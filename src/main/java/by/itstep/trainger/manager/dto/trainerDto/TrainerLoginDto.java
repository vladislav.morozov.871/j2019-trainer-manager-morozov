package by.itstep.trainger.manager.dto.trainerDto;

import lombok.Data;

@Data
public class TrainerLoginDto {

    private String email;

    private String password;

}
