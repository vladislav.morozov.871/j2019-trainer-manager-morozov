package by.itstep.trainger.manager.dto.trainerDto;

import by.itstep.trainger.manager.entity.Comment;
import by.itstep.trainger.manager.entity.Recording;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrainerCreateDto {

    private String name;

    private String surname;

    private int experience;

    private String achievements;

    private String email;

    private String password;
}
