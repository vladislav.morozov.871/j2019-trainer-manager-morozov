package by.itstep.trainger.manager.dto.trainerDto;

import by.itstep.trainger.manager.entity.Comment;
import by.itstep.trainger.manager.entity.Recording;
import lombok.Data;

import java.util.List;

@Data
public class TrainerUpdateDto {

    private Long id;

    private String name;

    private String surname;

    private int experience;

    private String achievements;

    private String password;
}
