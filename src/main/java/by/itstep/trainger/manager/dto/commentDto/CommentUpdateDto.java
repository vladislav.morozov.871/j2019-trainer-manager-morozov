package by.itstep.trainger.manager.dto.commentDto;

import by.itstep.trainger.manager.entity.Trainer;
import lombok.Data;

@Data
public class CommentUpdateDto {

    private Long id;

    private String message;

    private int mark;
}
