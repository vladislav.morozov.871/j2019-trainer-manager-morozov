package by.itstep.trainger.manager.dto.recordingDto;

import by.itstep.trainger.manager.entity.Trainer;
import lombok.Data;

import java.util.Date;

@Data
public class RecordingUpdateDto {

    private Long id;

    private String comment;

    private String name;

    private String surname;

    private String email;

    private int phone;

}
