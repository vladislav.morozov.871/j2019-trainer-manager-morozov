package by.itstep.trainger.manager.dto.adminDto;

import by.itstep.trainger.manager.enums.Role;
import lombok.Data;

import javax.persistence.Column;

@Data
public class AdminFullDto {

    private Long id;

    private String name;

    private String surname;

    private String email;

    private Role role;

}
