package by.itstep.trainger.manager.dto.commentDto;

import by.itstep.trainger.manager.entity.Trainer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
public class CommentFullDto {

    private Long id;

    private Trainer trainer;

    private String message;

    private String name;

    private String surname;

    private String email;

    private int mark;

    private boolean published;

}
