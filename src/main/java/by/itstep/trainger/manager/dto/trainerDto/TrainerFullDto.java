package by.itstep.trainger.manager.dto.trainerDto;

import by.itstep.trainger.manager.dto.commentDto.CommentFullDto;
import by.itstep.trainger.manager.dto.recordingDto.RecordingFullDto;
import by.itstep.trainger.manager.entity.Comment;
import by.itstep.trainger.manager.entity.Recording;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.util.List;

@Data
public class TrainerFullDto {

    private Long id;

    private String name;

    private String surname;

    private int experience;

    private String achievements;

    private List<Comment> comments;

    private String email;


    private List<Recording> recordings;

}
