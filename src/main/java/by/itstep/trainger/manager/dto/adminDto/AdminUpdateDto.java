package by.itstep.trainger.manager.dto.adminDto;

import by.itstep.trainger.manager.enums.Role;
import lombok.Data;

@Data
public class AdminUpdateDto {

    private Long id;

    private String name;

    private String surname;

    private String email;

    private String password;

}
