package by.itstep.trainger.manager.dto.commentDto;

import by.itstep.trainger.manager.entity.Trainer;
import lombok.Data;

@Data
public class CommentCreateDto {

    private String message;

    private String name;

    private String surname;

    private String email;

    private int mark;

    private Long trainerId;
}
