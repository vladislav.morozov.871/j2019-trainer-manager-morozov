package by.itstep.trainger.manager.dto.adminDto;

import lombok.Data;

@Data
public class AdminLoginDto {

    private String email;

    private String password;

}
