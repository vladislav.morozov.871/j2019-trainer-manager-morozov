package by.itstep.trainger.manager.dto.recordingDto;

import by.itstep.trainger.manager.entity.Trainer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
public class RecordingFullDto {

    private Long id;

    private Trainer trainer;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date recordingTime;

    private String comment;

    private String name;

    private String surname;

    private String email;

    private int phone;

}
