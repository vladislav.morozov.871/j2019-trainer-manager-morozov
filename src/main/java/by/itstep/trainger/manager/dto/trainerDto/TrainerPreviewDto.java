package by.itstep.trainger.manager.dto.trainerDto;

import by.itstep.trainger.manager.entity.Comment;
import by.itstep.trainger.manager.entity.Recording;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrainerPreviewDto {

    private Long id;

    private String name;

    private String surname;

    private int experience;

    private String email;

}
