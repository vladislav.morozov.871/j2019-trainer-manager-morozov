package by.itstep.trainger.manager.dto.recordingDto;

import by.itstep.trainger.manager.entity.Trainer;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class RecordingPreviewDto {

    private Long id;

    private String trainer;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date recordingTime;

    private String name;

    private String surname;
}
